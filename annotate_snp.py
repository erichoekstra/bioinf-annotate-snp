#!/usr/bin/env python3

"""
Program to determine whether a SNP in a sequence will alter the function of the protein.
It will grant a score based on two principles: The region it's in and if the amino acid
changes to an amino acid in a different category with a different function. It will give
a score based on this ranging from 0 to 10.

How to run:
1. Navigate, in the terminal, to the directory with the annotate_snp.py
2. To see the parameters to run do the following:
   $ python3 annotate_snp.py -h
3. Example to run
   $ python3 annotate_snp.py -n C -p 71 -m data/protein/MSA_alignmentfile.clustal_num
4. The output will be printed on the terminal!

"""

__autor__ = "Eric Hoekstra"
__version__ = 1.0

import sys
import argparse

CODON_DICT = {
    'ATA': 'I', 'ATC': 'I', 'ATT': 'I', 'ATG': 'M',
    'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACT': 'T',
    'AAC': 'N', 'AAT': 'N', 'AAA': 'K', 'AAG': 'K',
    'AGC': 'S', 'AGT': 'S', 'AGA': 'R', 'AGG': 'R',
    'CTA': 'L', 'CTC': 'L', 'CTG': 'L', 'CTT': 'L',
    'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCT': 'P',
    'CAC': 'H', 'CAT': 'H', 'CAA': 'Q', 'CAG': 'Q',
    'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGT': 'R',
    'GTA': 'V', 'GTC': 'V', 'GTG': 'V', 'GTT': 'V',
    'GCA': 'A', 'GCC': 'A', 'GCG': 'A', 'GCT': 'A',
    'GAC': 'D', 'GAT': 'D', 'GAA': 'E', 'GAG': 'E',
    'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGT': 'G',
    'TCA': 'S', 'TCC': 'S', 'TCG': 'S', 'TCT': 'S',
    'TTC': 'F', 'TTT': 'F', 'TTA': 'L', 'TTG': 'L',
    'TAC': 'Y', 'TAT': 'Y', 'TAA': '_', 'TAG': '_',
    'TGC': 'C', 'TGT': 'C', 'TGA': '_', 'TGG': 'W'}

def get_sequence(filename):
    """
    Function to parse the nucleotide sequence from the FASTA File
    :return: a string with the nucleotide sequence
    """
    file = open(filename)

    return "".join([line.strip() for line in file if not line.startswith('>')])


def get_old_triplet(snp_pos, sequence):
    """
    This function will find the old triplet before the SNP was done
    :return: old triplet string, triplet as list with each value a string,
             snp_position: The position of the SNP in the protein sequence
    """

    snp_position = snp_pos % 3

    # use this to select the triplet from the sequence as list (list aren't immutable)
    triplet = list(sequence[snp_pos - snp_position:snp_pos + (3 - snp_position)])
    old_triplet = "".join(triplet)
    return old_triplet, triplet, snp_position


def get_new_triplet(snp_nuc, snp_position, triplet):
    """
    This function uses the SNP on the protein and finds the new triplet according to the new codon.
    :return: The new triplet as a string
    """
    triplet[snp_position] = snp_nuc

    return "".join(triplet)

def compare_triplets(old_triplet, new_triplet):
    """
    This function will compare the triplets and check if they stay the same
    :return: True if they're the same. False if they're not the same
    """

    # Use the codon dict to see whether the amino acid changes
    if CODON_DICT[old_triplet] == CODON_DICT[new_triplet]:
        return True

    return False


def read_msa(filename):
    """
    :param filename: name of msa file
    :return msa_scores: found scores
    """
    # start with empty string
    scores_msa = ''

    # open file
    file = open(filename)

    for line in file:
        if line.startswith(' '):
            # The scores start at the 20th character
            scores_msa += line[20:len(line)].replace("\n", '')

    file.close()

    return scores_msa

def get_msa_score(snp_pos, msa_score):
    """
    This function will find the MSA score corresponding to the SNP position
    :return: the MSA score value
    """
    return msa_score[int(snp_pos / 3)]

def define_score(score):
    """
    This function will give an explanation per score
    :return: a score, the more conserved the region is the higher the score
    """

    if score == " ":
        print("The mutation was in a non-conserved region.")
        return 2
    if score == ':':
        print("The mutation was in a mildly conserved region.")
        return 4
    if score == '.':
        print("The mutation was in a more conserved region.")
        return 6
    if score == '*':
        print("The mutation was in a highly conserved region.")
        return 8

def compare_amino_acid(old_triplet, new_triplet):
    """
    This function will compare the first amino acid to the new amino acid and determine whether
    it's function is different
    :return: True or False depending on if the amino acid changes to an amino acid with a
             different function. True if it changes to amino acid in the same category.
             False if it changes to amino acid in other category
    """

    nonpolar = ["A", "V", "L", "G", "D", "I", "M", "W", "F", "P"]
    polar = ["S", "C", "N", "Z", "T", "Y"]
    acidic = ["B", "Z"]
    basic = ["K", "R", "H"]

    functionlist = [nonpolar, polar, acidic, basic]

    # IF new triplet in the same group als old triplet
    # Return "Function not altered to much, new aminoacid has the same function"

    for entry in functionlist:
        # Checking if the new triplet is in the same category als old triplet
        if new_triplet in entry and old_triplet in entry:
            print("The new aminoacid has the same function")
            return True

    print("The new aminoacid is different in function")
    return False

def argument_parser():
    """
    Argparse library to parse command line arguments from the command line easily
    :return parser:
    """
    # create parser
    parser = argparse.ArgumentParser(
        description="This script will check whether a SNP will have a big effect on "
                    "the function of the protein or not.",
        usage="python3 snp_annotation.py -n <SNP nucleotide>  -p <positie in het gen> -m <MSA>"
    )

    parser.add_argument('-n', '--SNP', type=str, metavar='', required=True,
                        help='The nucleotide it will change to, can be upper or lower case')
    parser.add_argument('-p', '--pos', type=int, metavar='', required=True,
                        help='The position of the point mutation')
    parser.add_argument('-m', '--MSA', type=str, metavar='', required=True,
                        help='A clustal file of the MSA')

    return parser.parse_args()

def main(args):
    """
    Main function
    :param args: The arguments given by the user1
    :return: 0
    """
    snp_pos = args.pos
    snp_nuc = args.SNP.upper()
    msa = args.MSA
    # A divider to use later on, makes it more user friendly
    divider = "----------------------------------------------------------"
    print(divider)
    # Show the user the parameters
    print("Parameters given: SNP position: {}, SNP nucleotide: {},\n"
          "MSA file location: {}".format(snp_pos, snp_nuc, msa))
    # Get the nucleotide sequence
    sequence = get_sequence('data/nucleotide/nucleotide.txt')

    # If the SNP position is to high the program needs to stop
    if snp_pos > len(sequence):
        print(divider, "\nERROR: SNP position bigger than the length of the sequence.")
        return

    # Get the old triplet
    old_triplet, triplet, snp_position = get_old_triplet(6, sequence)
    print(divider, "\nThe old triplet was: ", old_triplet)
    # Get the new triplet
    new_triplet = get_new_triplet(snp_nuc, snp_position, triplet)
    print("The new triplet is: ", new_triplet)
    # Compare the triplets, when the boolean is true there was no change
    boolean = compare_triplets(old_triplet, new_triplet)

    print(divider, "\nThe higher the score the bigger the impact on the protein.")

    if boolean:
        print("No change in amino acid was seen: score 0/10")
        return

    scores = read_msa(msa)
    # Get the score corresponding to the position in the sequence
    score = get_msa_score(snp_pos, scores)

    scores = define_score(score)

    if not compare_amino_acid(old_triplet, new_triplet):
        scores += 2

    print("Final score: {}/10".format(scores))
    print(divider)
    return 0


if __name__ == '__main__':
    EXITCODE = main(argument_parser())
    sys.exit(EXITCODE)
